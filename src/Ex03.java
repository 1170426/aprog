import java.util.Scanner;
public class Ex03 {
    public static double angulo;
    public static double ladoa, ladob, ladoc;
    public static void angulo(double ladoa, double ladob, double ladoc){
        double auxangulo = (Math.pow(ladoa, 2) + Math.pow(ladob, 2) - Math.pow(ladoc, 2))/(2*ladoa*ladob);
        angulo = Math.acos(auxangulo);
        System.out.println(Math.toDegrees(angulo) + "º");
        
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Primeiro lado: ");
        double lado1 = sc.nextDouble();
        System.out.print("Segundo lado: ");
        double lado2 = sc.nextDouble();
        System.out.print("Terceiro lado: ");
        double lado3 = sc.nextDouble();
        double maior = lado1;
        double soma = lado2 + lado3;
        if (lado2 > maior){
            maior = lado2;
            soma = lado1 + lado3;
        }else{
            if (lado3 > maior){
                maior = lado3;
                soma = lado1 + lado2;
            }
        }
        if (soma < maior){
            System.out.println("O triangulo em questão é inválido.");
        }else{
            System.out.println("Angulo(" + lado1 + ", " + lado2 + ")");
            angulo(lado1, lado2, lado3);
            System.out.println("Angulo(" + lado1 + ", " + lado3 + ")");
            angulo(lado1, lado3, lado2);
            System.out.println("Angulo(" + lado2 + ", " + lado3 + ")");
            angulo(lado2, lado3, lado1);
        }   
    }   
}
