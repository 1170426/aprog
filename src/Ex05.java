import java.util.Scanner;
public class Ex05 {
    public static int c, n1_o, n2_o, n1, n2;
    public static void digiguais(int n1, int n2){
        c = 0;
        n1_o = n1;
        n2_o = n2;
        while ((n1 != 0) || (n2 != 0)){
            int algan1 = n1 % 10;
            int algan2 = n2 % 10;
            if (algan1 == algan2){
                c++;
            }
            n1 /= 10;
            n2 /= 10;
        }
        System.out.println("");
        System.out.println(n1_o + " e " + n2_o + " têm " + c + " dígitos comuns nas mesmas posições.");
        System.out.println("");
    }    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = 0;
        int maior = c;
        int n1_m = 0;
        int n2_m = 0;
        do{
            System.out.print("Pares de valores inteiros positivos: ");
            N = sc.nextInt();
        }while (N <= 1);
        for (int i = 0;i < N;i++){
            do{
                System.out.print("Número 1: ");
                n1 = sc.nextInt();
            }while(n1 <= 0);
            do{
                System.out.print("Número 2: ");
                n2 = sc.nextInt();
            }while(n2 <= 0);
            digiguais(n1, n2);
            if (c > maior){
                maior = c;
                n1_m = n1_o;
                n2_m = n2_o;
            }
        }
        System.out.println("O par com mais dígitos em comum é (" + n1_m + ", " + n2_m + ") com " + maior + " dígitos em comum");
    } 
}
