import java.util.Scanner;
public class Ex06 {
    public static double raio, altura;
    public static void cone(double raio, double altura){
        double volume = (altura* Math.PI * Math.pow(raio, 2))/3;
        System.out.println("");
        System.out.println("O volume do cone é de " + volume);
        System.out.println("");
    }
    public static void esfera(double raio){
        double volume = (4/3)*Math.PI*Math.pow(raio, 3);
        System.out.println("");
        System.out.println("O volume da esfera é de " + volume);
        System.out.println("");
    }
    public static void cilindro(double raio, double altura){
        double volume = Math.PI*Math.pow(raio, 2)*altura;
        System.out.println("");
        System.out.println("O volume do cilindro é de " + volume);
        System.out.println("");
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Tipo de sólido de revolução: ");
        String tipo = sc.nextLine();
        while(!"FIM".equals(tipo)){
            switch (tipo){
                case "cone": 
                    System.out.print("Raio: ");
                    raio = sc.nextDouble();
                    System.out.print("Altura: ");
                    altura = sc.nextDouble();
                    cone(raio, altura);
                    break;
                case "esfera":
                    System.out.print("Raio: ");
                    raio = sc.nextDouble();
                    esfera(raio);
                    break;
                case "cilindro":
                    System.out.print("Raio: ");
                    raio = sc.nextDouble();
                    System.out.print("Altura: ");
                    altura = sc.nextDouble();
                    cilindro(raio, altura);
                    break;
            }
            Scanner sc2 = new Scanner(System.in);
            System.out.print("Tipo de sólido de revolução: ");
            tipo = sc2.nextLine();        
        }
    }
}
