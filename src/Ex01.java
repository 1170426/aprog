/*
a)
    Método 1:
        O módulo verifica se a palavra é palíndrome indo de caracter em caracter.
    Método 2:
        
*/
import javax.swing.JOptionPane;
public class Ex01 {
    public static boolean resposta;
    public static boolean metodo1(String pal){
        resposta = true;
        pal = pal.toLowerCase();
        int tamanho = pal.length();
        for (int i = 0;i < tamanho/2;i++){
            if (pal.charAt(i) != pal.charAt(tamanho - 1 - i)){
                resposta = false;
                break;
            }
        }
        return resposta;
    }
    public static void main(String[] args) {
        String palavra; 
        int c = 0;
        palavra = JOptionPane.showInputDialog("Palavra:");
        resposta = metodo1(palavra);
        while (resposta == false){
            c++;
            palavra = JOptionPane.showInputDialog("Palavra:");
            resposta = metodo1(palavra);
        }
        JOptionPane.showMessageDialog(null, c);
    } 
}
