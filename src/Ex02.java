import java.util.Scanner;
public class Ex02 {
    public static int alunos;
    public static void informacao(String disciplina){
        System.out.print("Número de alunos aprovados em " + disciplina + " : ");
        Scanner sc = new Scanner(System.in);
        int aprovados = sc.nextInt();
        String positivas = "- Positivas: ";
        String negativas = "- Negativas: ";
        String asterisco = "*";
        for(int i = 0;i < aprovados;i++){
            positivas += asterisco;
        }
        int reprovados = alunos - aprovados;
        for (int a = 0;a < reprovados;a++){
            negativas += asterisco;
        }
        System.out.println("");
        System.out.println(disciplina);
        System.out.println(positivas);
        System.out.println(negativas);
        System.out.println("");
    }    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Número de alunos: ");
        alunos = sc.nextInt();
        System.out.print("Número de disciplinas: ");
        int ndisciplina = sc.nextInt();
        for (int i = 0;i < ndisciplina;i++){
            System.out.print("Nome da disciplina: ");
            Scanner sc2 = new Scanner(System.in);
            String disciplina = sc2.nextLine();
            informacao(disciplina);
        }
    }   
}
