import java.util.Scanner;
public class Ex09 {
    public static String sucessao;
    public static String Fibonacci(int N){
        int numero1 = 0;
        int numero2 = 1;
        sucessao = "0, 1, ";
        for (int i = 0;i < (N-2);i++){
            int numero3 = numero1 + numero2;
            String numeroS = Integer.toString(numero3);
            sucessao += numeroS + ", ";
            numero1 = numero2;
            numero2 = numero3;
        }
        return sucessao;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = 0;
        do{
        System.out.print("Quantidade (>= 2) de termos da sucessão de Fibonacci: ");
        N = sc.nextInt();
        }while(N < 2);
        sucessao = Fibonacci(N);
        System.out.println("");
        System.out.print(sucessao);
    } 
}
