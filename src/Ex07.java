import java.util.Scanner;
public class Ex07 {
    public static boolean resposta;
    public static int novo;
    public static boolean octal(int num){
        resposta = true;
        while(num != 0){
            int alga = num % 10;
            if ((alga > 7) || (alga < 0)){
                System.out.print("O número em questão não está em base octal.");
                resposta = false;
                break;
            }else{
                num /= 10;
            }
        }
        return resposta;
    }
    public static int conversao(int num){
        int ordem = 0;
        novo = 0;
        while(num != 0){
            int alga = num % 10;
            novo = (int) (novo + alga * Math.pow(8, ordem));
            ordem++;
            num /= 10;
        }
        return novo;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Número em base octal: ");
        int num = sc.nextInt();
        resposta = octal(num);
        while (resposta != false){
            novo = conversao(num);
            System.out.println("O número introduzido convertido para base decimal fica: " + novo);
            System.out.println("");
            System.out.print("Número em base octal: ");
            num = sc.nextInt();
            resposta = octal(num);
        }
        
    }
    
}
