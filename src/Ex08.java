import java.util.Scanner;
public class Ex08 {
    public static String hexadecimal;
    public static String hexa(int decimal){
        String letra = "";
        String novo = ""; 
        while (decimal != 0){
            int alga = decimal % 16;
            if (alga >= 10){
                switch (alga){
                    case 10:
                        letra = "A";
                        break;
                    case 11:
                        letra = "B";
                        break; 
                    case 12:
                        letra = "C";
                        break;
                    case 13:
                        letra = "D";
                        break;
                    case 14:
                        letra = "E";
                        break;
                    case 15:
                        letra = "F";
                        break;
                }
                novo += letra;
            }else{
                String algas = Integer.toString(alga);
                novo += algas;
            }
            decimal /= 16;
        }
        hexadecimal = new StringBuilder(novo).reverse().toString();
        return hexadecimal;
        }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduza um número décimal: ");
        int decimal = sc.nextInt();
        System.out.println(" ");
        while(decimal != 0){
            hexadecimal = hexa(decimal);
            System.out.println("O número " + decimal + " em base hexadécimal é igual a: " +hexadecimal);
            System.out.println(" ");
            System.out.print("Introduza um número décimal: ");
            decimal = sc.nextInt();
            System.out.print(" ");
        }
    }  
}
